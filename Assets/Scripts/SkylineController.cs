﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkylineController : MonoBehaviour
{
    public GameObject skyline;
    int offset = 100;
    GameObject[] towers = new GameObject[14];
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < towers.Length; i++)
            towers[i] = Instantiate(skyline, this.gameObject.transform.position + new Vector3(offset * ((i % 2 * 2) - 1), Random.Range(-50, -10), i / 2 * 30 + 40), Quaternion.identity); ;
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GameObject tower in towers)
        {
            if (tower.transform.position.z - this.gameObject.transform.position.z < 35)
            {
                tower.transform.position = new Vector3(tower.transform.position.x, Random.Range(-50, -10) - 80, tower.transform.position.z + towers.Length * 15);
                tower.GetComponent<TowerController>().Rise();
            }
            if (tower.transform.position.z - this.gameObject.transform.position.z > towers.Length * 15 + 50)
            {
                tower.transform.position = new Vector3(tower.transform.position.x, Random.Range(-50, -10) - 80, tower.transform.position.z - towers.Length * 15);
                tower.GetComponent<TowerController>().Rise();
            }
        }
    }
}
