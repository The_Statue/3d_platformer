﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerController : MonoBehaviour
{
    bool rise = false;
    float height = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (rise == true)
        {
            this.transform.position += new Vector3(0, 15 * Time.deltaTime, 0);
            if (this.gameObject.transform.position.y > height + 80)
                rise = false;
        }
    }

    public void Rise()
    {
        height = this.gameObject.transform.position.y;
        rise = true;
    }
}
