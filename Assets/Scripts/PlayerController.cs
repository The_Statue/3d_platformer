﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    CharacterController playerController;
    bool grounded = false;
    int speed = 5;
    public float jumpHeight = 5.5f;
    public int checkCount = 0;
    float velocityY = 0;
    int layerMask;
    Vector3 respawnPoint = new Vector3(0, 1, 0);
    Animator animController;
    public Joystick stick;

    // Start is called before the first frame update
    void Start()
    {
        animController = this.gameObject.transform.GetChild(2).GetComponent<Animator>();
        playerController = this.gameObject.GetComponent<CharacterController>();
        layerMask = 1 << 8;
        layerMask = ~layerMask;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
            jump();

        //Get axis movement and update player controller
        Vector3 moveV = new Vector3(Input.GetAxis("Horizontal") + stick.Horizontal, 0, Input.GetAxis("Vertical") + stick.Vertical);
        //moveV = Vector3.Normalize(moveV);
        playerController.Move(moveV * Time.deltaTime * speed);

        if (moveV != Vector3.zero) // camera rotation
        {
            //gameObject.transform.forward = moveV; Get better camera control
            animController.SetBool("Moving", true);
        }
        else
            animController.SetBool("Moving", false);

        //Gravity
        if (!grounded)
            velocityY += -9.8f * Time.deltaTime;
        moveV = new Vector3(0, velocityY, 0);
        playerController.Move(moveV * Time.deltaTime);
        // ----Ground detection
        RaycastHit hit;
        if (grounded == false && velocityY < 1 && Physics.Raycast(transform.position, transform.TransformDirection(Vector3.down), out hit, 1.02f, layerMask)) //Check if not grounded, check if falling, check if hit
        {
            if (hit.collider != null)
            {
                hit.collider.gameObject.GetComponent<PlatformController>().Hit();
                int check = hit.collider.gameObject.GetComponent<PlatformController>().count;
                if (checkCount < check)
                {
                    checkCount = check;
                    //Get position for respawn (run only when a new checkCount is found to save on calculations
                    respawnPoint.z += checkCount * 6;
                    respawnPoint.y += checkCount * 1;
                }
                grounded = true;
                animController.SetBool("Grounded", true);
                animController.SetInteger("State", 0);
            }
        }


        //Reset
        if (gameObject.transform.position.y < respawnPoint.y - 10)
            gameObject.transform.position = respawnPoint;
    }

    public void jump()
    {
        if (grounded == false)
            return;
        animController.SetInteger("State", 2);
        velocityY = jumpHeight;
        grounded = false;
        animController.SetBool("Grounded", false);
    }
}
