﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformController : MonoBehaviour
{
    public GameObject prefab;
    public GameObject flagPrefab;
    GameObject UI;
    public int count = 0;
    public int targetHeight = -25;
    public bool touched = false; //will be used as polymorphism for stepping platforms and checkpoint platforms
    float transition = 0;
    private int offset = 6;
    GameObject flagObject;

    public Material material1;
    public Material material2;
    public float platformDuration = 6.0f;
    float currentDuration = 0.0f;
    Renderer rend;
    // Start is called before the first frame update
    public void Begin()
    {
        if (touched == false)
        {
            this.gameObject.name = "checkPoint " + count;
            this.gameObject.transform.localScale = new Vector3(4, 50, 4);
            flagObject = Instantiate(flagPrefab, this.gameObject.transform.position + new Vector3(-1.35f, 24.95f, 1.35f), Quaternion.Euler(-90, 180, 0));
            flagObject.transform.SetParent(this.gameObject.transform);
            flagObject.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = count.ToString();
            Debug.Log(flagObject.transform.position.y + " " + targetHeight);
        }
        else
        {
            this.gameObject.name = "step " + count;
            this.gameObject.transform.localScale = new Vector3(2, 50, 2);
        }
        platformDuration = count + 10; //allocate 1 second for every extra platform
        //offset = count + 8; //Increase distance between checkpoints?
    }

    void Start()
    {
        UI = GameObject.FindGameObjectsWithTag("UI")[0];
        rend = GetComponent<Renderer>();

        // At start, use the first material
        rend.material = material1;
    }

    void Update()
    {
        if (touched == true) //Decay material over time, only on platforms and checkpoints that have been reached
        {
            currentDuration += Time.deltaTime;
            float lerp = currentDuration / platformDuration;
            rend.material.Lerp(material1, material2, lerp);
            if (lerp >= 1)
            {
                targetHeight = -100;
                transition = 0;
            }
        }

        if (transition < 5f)
        {
            if (gameObject.transform.position.y < targetHeight)
                gameObject.transform.position += new Vector3(0, 60 * Time.deltaTime, 0);
            if (gameObject.transform.position.y > targetHeight)
                gameObject.transform.position -= new Vector3(0, 60 * Time.deltaTime, 0);
            if (gameObject.transform.position.y <= -100)
            {
                Destroy(this.gameObject);
                UI.GetComponent<UIController>().recordCheck();
            }
            transition += Time.deltaTime;
            if (transition >= 2f)
                gameObject.transform.position = new Vector3(gameObject.transform.position.x, targetHeight, gameObject.transform.position.z);
        }
    }

    public void Hit()
    {
        if (touched == true)
            return;

        if (true) // check for player for redundency?
        {
            Destroy(flagObject);
            touched = true;
            Debug.Log(count + 1);
            float deviation = 0;
            for (int i = 0; i < count + 1; i++)
            {
                Debug.Log(i);

                deviation = Random.Range(-4.5f, 4.5f) + deviation; // deviate based on last position, allowing steps to follow up more naturally and ensure every step can be jumped to

                //Check if steps get to far away from center, if so, truncate (useful to ensure second to last step and final step can make it back to checkpoint)
                if (deviation > 70)
                    deviation = 70;
                if (deviation < -70)
                    deviation = -70;
                // Check if its possible to reach from both checkpoints based on the number of steps
                if (deviation > (count - i) * 5)
                {
                    Debug.Log("Deviation:" + deviation + " Maximum: " + (count - i) * 5);
                    deviation = (count - i) * 5;
                }
                else if (deviation < (count - i) * -5)
                {
                    Debug.Log("Deviation:" + deviation + " Maximum: " + (count - i) * -5);
                    deviation = (count - i) * -5;
                }
                StartCoroutine(SpawnAfterDelay(i * 0.25f, i, deviation));
            }
        }

    }

    IEnumerator SpawnAfterDelay(float time, int loopCount, float dev)
    {
        yield return new WaitForSeconds(time);

        // Code to execute after the delay
        GameObject platform = Instantiate(prefab, this.gameObject.transform.position + new Vector3(0, -25, offset * (loopCount + 1)), this.gameObject.transform.rotation);
        Debug.Log(string.Format("Mouse Position: {0} Move target: {1}", platform.transform.position.y, targetHeight + loopCount + 1));
        if (loopCount == count)
        {
            platform.GetComponent<PlatformController>().count = count + 1;
            platform.GetComponent<PlatformController>().touched = false;
        }
        else
        {
            platform.GetComponent<PlatformController>().count = count;

            platform.transform.position += new Vector3(dev, 0, 0); // Maybe allow check points to move? would need to rework respawnpoint to figure out x pos
        }
        platform.GetComponent<PlatformController>().targetHeight = targetHeight + loopCount + 1;

        platform.GetComponent<PlatformController>().Begin();
    }
}
