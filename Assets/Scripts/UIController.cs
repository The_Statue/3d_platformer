﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    GameObject MainMenu;
    GameObject OptionsMenu;

    GameObject MobileControls;

    GameObject SatanSlider;
    GameObject HumanSlider;

    Toggle EasyToggle;
    Toggle RegularToggle;
    Toggle HardToggle;

    Toggle FogToggle;
    Toggle JoyStickToggle;

    public GameObject Player;
    GameObject Fog;
    bool value = false;
    bool EnableMobile = false;
    int CheckCounter = 0;
    int endLength = 10;

    // Start is called before the first frame update
    void Start()
    {
        MainMenu = this.gameObject.transform.GetChild(0).gameObject;
        MainMenu.transform.GetChild(0).gameObject.GetComponent<Button>().onClick.AddListener(StartFunction);
        MainMenu.transform.GetChild(1).gameObject.GetComponent<Button>().onClick.AddListener(OptionsFunction);
        MainMenu.transform.GetChild(2).gameObject.GetComponent<Button>().onClick.AddListener(ExitFunction);


        Fog = Player.transform.GetChild(1).gameObject;


        OptionsMenu = this.gameObject.transform.GetChild(1).gameObject;

        EasyToggle = OptionsMenu.transform.GetChild(1).gameObject.GetComponent<Toggle>();
        EasyToggle.onValueChanged.AddListener((value) => EasyFunction(value));

        RegularToggle = OptionsMenu.transform.GetChild(2).gameObject.GetComponent<Toggle>();
        RegularToggle.onValueChanged.AddListener((value) => RegularFunction(value));

        HardToggle = OptionsMenu.transform.GetChild(3).gameObject.GetComponent<Toggle>();
        HardToggle.onValueChanged.AddListener((value) => HardFunction(value));

        FogToggle = OptionsMenu.transform.GetChild(5).gameObject.GetComponent<Toggle>();
        FogToggle.onValueChanged.AddListener((value) => FogFunction(value));

        JoyStickToggle = OptionsMenu.transform.GetChild(6).gameObject.GetComponent<Toggle>();
        JoyStickToggle.onValueChanged.AddListener((value) => JoyStickFunction(value));

        MobileControls = this.transform.GetChild(4).gameObject;

        OptionsMenu = this.gameObject.transform.GetChild(1).gameObject;
        OptionsMenu.transform.GetChild(4).gameObject.GetComponent<Button>().onClick.AddListener(Pause);

        SatanSlider = this.gameObject.transform.GetChild(2).gameObject;
        HumanSlider = this.gameObject.transform.GetChild(3).gameObject;

        if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WebGLPlayer)
        {
            JoyStickToggle.isOn = true;
            FogToggle.isOn = false;
        }


        SatanSlider.transform.GetChild(2).GetChild(0).gameObject.SetActive(false);//hide satan face
        SatanSlider.transform.GetChild(1).GetChild(0).gameObject.SetActive(false);//hide satan fill
        Pause();
    }

    // Update is called once per frame
    void Update()
    {
        HumanSlider.GetComponent<Slider>().value = Player.transform.position.z / (endLength * ((endLength * 3) + 3));
        if (Player.transform.position.z > (endLength * ((endLength * 3) + 3))) //Set the next milestone in increments of 10
        {
            endLength += 10;
            SatanSlider.GetComponent<Slider>().value = (float)CheckCounter / (endLength * ((endLength * 3) + 3)); //Update satan slider (only updates itself when there is a value change)
        }
        if (Input.GetButtonDown("Cancel"))
            Pause();
        if ((float)(CheckCounter - 1) / (endLength * ((endLength * 3) + 3)) >= Player.transform.position.z / (endLength * ((endLength * 3) + 3)))
            Application.LoadLevel(Application.loadedLevel);
    }

    public void Pause()
    {
        Time.timeScale = 0;

        MainMenu.SetActive(true);
        OptionsMenu.SetActive(false);

        MobileControls.SetActive(false);

        SatanSlider.SetActive(false);
        HumanSlider.SetActive(false);
    }

    void StartFunction()
    {
        Time.timeScale = 1;

        MainMenu.SetActive(false);
        OptionsMenu.SetActive(false);

        MobileControls.SetActive(EnableMobile);

        SatanSlider.SetActive(true);
        HumanSlider.SetActive(true);
    }
    void EasyFunction(bool value)
    {
        if (value == false)
        {
            EasyToggle.SetIsOnWithoutNotify(true);
            return;
        }
        RegularToggle.SetIsOnWithoutNotify(false);
        HardToggle.SetIsOnWithoutNotify(false);
        Player.GetComponent<PlayerController>().jumpHeight = 6;
    }
    void RegularFunction(bool value)
    {
        if (value == false)
        {
            RegularToggle.SetIsOnWithoutNotify(true);
            return;
        }
        EasyToggle.SetIsOnWithoutNotify(false);
        HardToggle.SetIsOnWithoutNotify(false);
        Player.GetComponent<PlayerController>().jumpHeight = 5.5f;
    }
    void HardFunction(bool value)
    {
        if (value == false)
        {
            HardToggle.SetIsOnWithoutNotify(true);
            return;
        }
        EasyToggle.SetIsOnWithoutNotify(false);
        RegularToggle.SetIsOnWithoutNotify(false);
        Player.GetComponent<PlayerController>().jumpHeight = 5f;
    }
    void FogFunction(bool value)
    {
        if (value == false)
        {
            Fog.SetActive(false);
            return;
        }
        Fog.SetActive(true);
    }
    void JoyStickFunction(bool value)
    {
        if (value == false)
        {
            EnableMobile = false;
            return;
        }
        EnableMobile = true;
    }
    void OptionsFunction()
    {
        MainMenu.SetActive(false);
        OptionsMenu.SetActive(true);
    }
    void ExitFunction()
    {
        Application.Quit();
    }

    public void recordCheck()
    {
        CheckCounter += 6;
        SatanSlider.transform.GetChild(2).GetChild(0).gameObject.SetActive(true);//show satan face
        SatanSlider.transform.GetChild(1).GetChild(0).gameObject.SetActive(true);//show satan fill
        SatanSlider.GetComponent<Slider>().value = (float)CheckCounter / (endLength * ((endLength * 3) + 3));
    }
}
