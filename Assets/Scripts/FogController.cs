﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FogController : MonoBehaviour
{
    Transform fog;
    public float targetHeight = 0;
    // Start is called before the first frame update
    void Start()
    {
        fog = this.gameObject.transform;
        targetHeight = fog.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        if (fog.position.y != targetHeight)
            fog.position = new Vector3(fog.position.x, targetHeight, fog.position.z);
    }
}
